# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Show overlays with images and text in Wayland"
HOMEPAGE="https://git.sr.ht/~midgard/wl-overlay"
if [[ ${PV} == 9999* ]]; then
	EGIT_REPO_URI="https://git.sr.ht/~midgard/${PN}.git"
	inherit git-r3
else
	KEYWORDS="amd64 x86"
	SRC_URI="https://git.sr.ht/~midgard/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	S="${WORKDIR}/${PN}-v${PV}"
fi

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

REQUIRED_USE=""

COMMON_DEPEND="
	dev-libs/wayland
	x11-libs/cairo
	x11-libs/pango
	"
DEPEND="$COMMON_DEPEND
	dev-util/wayland-scanner
	dev-libs/wayland-protocols
	"
RDEPEND="${COMMON_DEPEND}
	"
BDEPEND="
	"

src_install() {
	emake DESTDIR="$D" PREFIX=/usr install
}
