# midgard

Gentoo [overlay](https://wiki.gentoo.org/wiki/Ebuild_repository) for random software that I use.

## Installation
Installation using [Layman](https://wiki.gentoo.org/wiki/Layman) is easy.
```
layman -f -o https://framagit.org/Midgard/gentoo-overlay/raw/master/overlay.xml -a midgard
```
