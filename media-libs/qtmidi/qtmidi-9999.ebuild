# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 qmake-utils

DESCRIPTION="Platform independent MIDI Qt plugin"
HOMEPAGE="https://gitlab.com/tp3/qtmidi"
EGIT_REPO_URI="https://gitlab.com/tp3/${PN}.git"
if [[ ${PV} == *9999 ]]; then
	KEYWORDS=""
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64"
fi
LICENSE="GPL-3+"
SLOT="0"

IUSE=""
REQUIRED_USE=""

RDEPEND="
	dev-qt/qtgui:5
	dev-qt/qtdeclarative:5
	"
DEPEND="${RDEPEND}"

src_configure() {
	eqmake5
}

src_install() {
	emake INSTALL_ROOT="${D}" install
}
