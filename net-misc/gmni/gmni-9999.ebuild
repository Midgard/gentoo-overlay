# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="curl-like utility and terminal browser for Gemini"
HOMEPAGE="https://sr.ht/~sircmpwn/gmni/"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://git.sr.ht/~sircmpwn/${PN}"
else
	die "Releases not yet supported in ebuild"
fi

LICENSE="GPL-3"
SLOT="0"
IUSE="+man"

PATCHES=(
	"${FILESDIR}"/${P}-destdir.patch
	"${FILESDIR}"/${P}-man-option.patch
)

DEPEND="
	dev-libs/openssl
"
RDEPEND="${DEPEND}"
BDEPEND="
	virtual/pkgconfig
	man? ( app-text/scdoc )
"

src_configure() {
	econf \
		$(use_enable man)
}
