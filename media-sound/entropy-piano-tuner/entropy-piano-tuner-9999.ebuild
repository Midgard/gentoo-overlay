# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop git-r3 qmake-utils xdg

DESCRIPTION="Entropy-based piano tuning application"
HOMEPAGE="http://piano-tuner.org/"
EGIT_REPO_URI="https://gitlab.com/tp3/${PN}"
if [[ ${PV} == *9999 ]]; then
	KEYWORDS=""
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64"
fi
LICENSE="GPL-3+"
SLOT="0"

IUSE=""
REQUIRED_USE=""

RDEPEND="
	dev-libs/libuv
	media-libs/alsa-lib
	media-libs/qtmidi
	>=sci-libs/fftw-3
	x11-libs/qwt:5
	"
DEPEND="${RDEPEND}"

PATCHES=(
	"${FILESDIR}/update-qwt-library-name.patch"
)

src_prepare() {
	default
	rm -rf thirdparty/{fftw3,libuv,qwt-lib}
}

src_configure() {

	_qmake() {
		# static_core because the application couldn't find its libcore.so in
		# /usr/lib64/entropypianotuner for some reason
		eqmake5 \
			EPT_CONFIG+=static_core \
			PREFIX=/usr \
			PKGDIR="${D}/usr/" \
			EPT_INSTALL_LIB_RDIR="$(get_libdir)" \
			EPT_INSTALL_DATA_RDIR=share \
			EPT_THIRDPARTY_CONFIG+=system_qwt \
			EPT_THIRDPARTY_CONFIG+=system_fftw3 \
			EPT_THIRDPARTY_CONFIG+=system_libuv
	}

	_qmake

	# Explicitly configure app/ too because otherwise this is configured in the compilation step
	# without proper configuration of paths
	pushd app
	_qmake
	popd
}

pkg_preinst() {
	xdg_pkg_preinst
}
pkg_postinst() {
	xdg_pkg_postinst
}
pkg_postrm() {
	xdg_pkg_postrm
}
