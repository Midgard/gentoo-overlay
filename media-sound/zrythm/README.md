Dependencies from flewkey-overlay:
- dev-libs/libcyaml
- media-libs/lsp-dsp-lib

Dependencies included in my repository:
- media-fonts/dseg
- sys-libs/libbacktrace

Further dependency notes:
- At the time of writing, dependency media-sound/carla-2.6.0 is not yet released by the Carla team, use media-sound/carla-9999
