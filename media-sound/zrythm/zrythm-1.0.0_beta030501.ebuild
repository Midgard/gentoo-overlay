# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson xdg gnome2-utils

# Arcane fiddling with the version in pure Bash because external tools not allowed at this point
# and we must convert Gentoo compatible to upstream's complicated version scheme, so for example:
# PV=1.0.0_beta040001 becomes MY_PV=1.0.0-beta4.0.1
if [[ $PV =~ ^(.*)_(alpha|beta)([0-9][0-9])([0-9][0-9])([0-9][0-9])$ ]]; then
	MY_PV="${BASH_REMATCH[1]}-${BASH_REMATCH[2]}.${BASH_REMATCH[3]}.${BASH_REMATCH[4]}.${BASH_REMATCH[5]}"
	while [[ $MY_PV =~ ^(.*\.)0([0-9].*) ]]; do
		MY_PV="${BASH_REMATCH[1]}${BASH_REMATCH[2]}"
	done
fi

DESCRIPTION="Digital Audio Workstation"
HOMEPAGE="https://www.zrythm.org/"
if [[ ${PV} == 9999* ]]; then
	EGIT_REPO_URI="https://gitlab.zrythm.org/zrythm/${PN}.git"
	inherit git-r3
else
	SRC_URI="https://www.zrythm.org/releases/${PN}-${MY_PV}.tar.xz"
	KEYWORDS="~amd64"
	S="${WORKDIR}/${PN}-${MY_PV}"
fi

LICENSE="AGPL-3+"
SLOT="0"
IUSE="+man doc alsa +graphviz +guile jack lsp-dsp opus +plugins portaudio pulseaudio rtaudio rtmidi sdl test X"

DEPEND="
	app-arch/zstd
	dev-libs/glib
	dev-libs/json-glib
	dev-libs/libcyaml
	dev-libs/libpcre2
	dev-libs/reproc
	dev-libs/xxhash
	dev-libs/zix
	gui-libs/gtk:4
	gui-libs/gtksourceview:5
	gui-libs/libadwaita
	gui-libs/libpanel
	kde-frameworks/breeze-icons
	<media-fonts/dseg-0.50
	media-libs/libaudec
	media-libs/lilv
	media-libs/rubberband
	media-libs/vamp-plugin-sdk
	media-sound/carla
	net-misc/curl
	sci-libs/fftw
	sys-libs/libbacktrace

	alsa? ( media-libs/alsa-lib )
	graphviz? ( media-gfx/graphviz )
	guile? ( dev-scheme/guile )
	jack? ( virtual/jack )
	lsp-dsp? ( media-libs/lsp-dsp-lib )
	opus? ( media-libs/libsndfile )
	portaudio? ( media-libs/portaudio )
	pulseaudio? ( media-sound/pulseaudio )
	plugins? ( dev-libs/boost )
	rtaudio? ( media-libs/rtaudio )
	rtmidi? ( media-libs/rtmidi )
	sdl? ( media-libs/libsdl2 )
	"
# TODO Package media-libs/lsp-dsp-lib
# TODO dev-libs/libcyaml is in flewkey-overlay
# TODO media-libs/libaudec is in flewkey-overlay
RDEPEND="${DEPEND}"
BDEPEND="
	dev-lang/sassc
	dev-scheme/guile
	"

PATCHES=(
	"${FILESDIR}/${P}-fix-compilation.patch"
)

src_configure() {
	local emesonargs=(
		-Dcheck_updates=false
		-Ddseg_font=false
		-Dcarla_binaries_dir=/usr/lib64/carla

		$(meson_use man manpage)
		# TODO Needs sphinx-intl which isn't packaged yet
		#$(meson_use doc user_manual)
		-Duser_manual=false

		# Note from 1.0.0-beta3.5.1: Building without Carla makes build fail in linking phase
		-Dcarla=enabled

		$(meson_feature alsa)
		$(meson_feature graphviz)
		$(meson_feature guile)
		$(meson_feature jack)
		$(meson_feature lsp-dsp lsp_dsp)
		$(meson_use opus)
		$(meson_use plugins bundled_plugins)
		$(meson_feature portaudio)
		$(meson_feature pulseaudio pulse)
		$(meson_feature rtaudio)
		$(meson_feature rtmidi)
		$(meson_feature sdl)
		$(meson_use test tests)
		$(meson_use test gui_tests)
		$(meson_feature X x11)
	)
	meson_src_configure
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
	elog "To run Zrythm, a D-Bus user session must be running. Else Zrythm will be stuck in a boot loop."
}
pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}
