# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit java-pkg-2 java-ant-2

DESCRIPTION="Analyze and experiment with pitch organization in music"
HOMEPAGE="https://0110.be/Software"
SRC_URI=""

if [[ ${PV} == 9999* ]]; then
	EGIT_REPO_URI="https://github.com/JorenSix/Tarsos.git"
	inherit git-r3
else
	die "Upstream does not have recent releases, this ebuild supports only version 9999"
fi

S="${WORKDIR}/${P}/build"


LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="doc"

COMMON_DEP=""
RDEPEND="${COMMON_DEP}
>=virtual/jre-1.8"
DEPEND="${COMMON_DEP}
>=virtual/jdk-1.8"


EANT_BUILD_TARGET="create_run_jar"
# java-pkg-2 takes care of building target “javadoc” if “doc” USE flag is set

src_prepare() {
	sed -i 's/\${user\.name}/Gentoo ebuild/' build.xml
	default
}

src_install() {
	jarfile="$(echo Tarsos-*.jar)"
	java-pkg_dojar "$jarfile"

	java-pkg_dolauncher "${PN}" --jar "$jarfile"

	if use doc; then
		java-pkg_dojavadoc ../doc
	fi

	default
}
