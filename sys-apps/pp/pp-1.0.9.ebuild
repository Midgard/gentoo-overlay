# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Simple preprocessor that allows embedding sh code in files of any type"
HOMEPAGE="https://mkws.sh/pp.html"
SRC_URI="https://mkws.sh/pp/${PN}@${PV}.tgz"
S="${WORKDIR}/${PN}"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

REQUIRED_USE=""

RDEPEND=""
DEPEND="${RDEPEND}
"
BDEPEND="
"

src_install() {
	emake PREFIX=/usr DESTDIR="$D" install
}
