# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Font families that imitate 7- and 14-segment displays"
HOMEPAGE="https://www.keshikan.net/fonts-e.html"
SRC_URI="https://github.com/keshikan/DSEG/releases/download/v${PV}/fonts-DSEG_v${PV//./}.zip -> ${P}.zip"
S="${WORKDIR}/fonts-DSEG_v${PV//./}"

LICENSE="AGPL-3+"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_compile() {
	:
}

src_install() {
	local destdir="${D}/usr/share/fonts/dseg"
	find -name "*.ttf" | while read file; do
		install -T -D "$file" "$destdir/$file"
	done
}
